# basedir is set by <lang>/conf.py
"""
Use "-D language=<LANG>" option to build a localized paraview-docs document.
For example::

    sphinx-build -D language=ja -b html . _build/html

This conf.py do:

- Specify `locale_dirs`.
- Overrides source directory as 'paraview-docs/doc/source`.

"""
import datetime
import os
import pathlib

basedir = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "paraview-docs/doc/source"
)
exec(pathlib.Path(os.path.join(basedir, "conf.py")).read_text(), globals())
locale_dirs = [os.path.join(basedir, "../../../locale/")]
# General information about the project.
project = 'ParaView Documentation'
year = datetime.date.today().year
copyright = f"2021-{year}, The Open CAE Society of Japan"
author = 'The Open CAE Society of Japan'


def setup(app):
    app.srcdir = basedir
    app.confdir = app.srcdir
