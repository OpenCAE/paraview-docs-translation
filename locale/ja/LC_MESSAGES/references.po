# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, ParaView Developers
# This file is distributed under the same license as the ParaView Documentation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# k nakayama <nak954@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ParaView Documentation 5.12\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-05 07:02+0000\n"
"PO-Revision-Date: 2021-11-16 06:31+0000\n"
"Last-Translator: k nakayama <nak954@gmail.com>, 2021\n"
"Language-Team: Japanese (https://app.transifex.com/the-open-cae-society-of-japan-2/teams/126975/ja/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ja\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../paraview-docs/doc/source/references.rst:2
msgid "References"
msgstr "リファレンス"
