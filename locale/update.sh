#!/bin/sh
# update transifex pot and local po files

set -ex

# pull po files from transifex
cd `dirname $0`
echo $SPHINXINTL_TRANSIFEX_USERNAME
echo $SPHINXINTL_TRANSIFEX_PROJECT_NAME
#rm -R pot  # skip this line cause "already unused pot files will not removed" but we must keep these files to avoid commit for only "POT-Creation-Time" line updated. see: https://github.com/sphinx-doc/sphinx/issues/3443
ls -l ./pot
sphinx-build -T -b gettext ../paraview-docs/doc/source pot
sphinx-intl update-txconfig-resources -p pot -d .
cat .tx/config
tx push -s --skip && rm -Rf ja
tx pull --silent -f -l ja
git checkout .tx/config
