# <img src="icon.png" width="50"> Translated docs for ParaView official document

[English](README.md) | [日本語](README.ja.md)

[![Documentation Status](https://readthedocs.org/projects/paraview-ja/badge/?version=latest)](https://paraview-ja.readthedocs.io/ja/latest/?badge=latest)

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

This is a project to provide paraview official documentation with multiple versions and multiple languages on Read The Docs site.

The current procedure is a bit tricky because Read The Docs doesn't have a way to specify options for sphinx-build command.
conf.py files for each language have 'language' and 'locale_dirs' values without having a full copy of conf.py of sphinx doc.
If we want to specify conf.py file that is out of the source directory, we will use '-c' option for sphinx-build command.
Unfortunately, Read the Docs can't.
If there is any better way, please let me know.

## URLs

* RTD project pages for Paraview:

  * https://readthedocs.org/projects/paraview/  (Master)
  * https://readthedocs.org/projects/paraview-ja/

* Documentation pages for each language:

  * https://paraview.readthedocs.io/en/latest/
  * https://paraview.readthedocs.io/ja/latest/

## How to setup a translated documentation project on RTD

The detail is here: https://docs.readthedocs.org/en/latest/localization.html#project-with-multiple-translations

Points are:

* We must have RTD projects for each language.
* Each project must have the correct Language setting on the "Settings" page.
* Master project connects to each translated project on the "translations settings" page.


## How to update po files

```
sh ./locale/update.sh
```

After that, you should commit updated po files.


## How to add a language

1. add language to locale/update.sh:

   ```
   - rm -R ja
   - tx pull -l ja
   + rm -R ja de
   + tx pull -l ja,de
   ```

1. update po files

1. commit them

1. add new project on Read The Docs like:

   https://readthedocs.org/projects/paraview-ja/

1. add translation project to parent project like:

   https://readthedocs.org/dashboard/paraview/translations/

<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">Creative Commons Attribution-NoDerivatives 4.0 International License</a>.
