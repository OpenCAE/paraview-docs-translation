# <img src="icon.png" width="50"> ParaView公式ドキュメントの翻訳ドキュメント

[English](README.md) | [日本語](README.ja.md)

[![Documentation Status](https://readthedocs.org/projects/paraview-ja/badge/?version=latest)](https://paraview-ja.readthedocs.io/ja/latest/?badge=latest)

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.ja.md)

これは、Read The Docsというサイトで、paraviewの公式ドキュメントを多版・多言語で提供するプロジェクトです。

Read The Docsには、sphinx-buildコマンドのオプションを指定する方法がないため、現在の手順では少し厄介なことになっています。
各言語の conf.py ファイルには 'language' と 'locale_dirs' の値があり、sphinx doc の conf.py の完全なコピーがない場合、この値が使われます。
ソースディレクトリ外の conf.py ファイルを指定したい場合は、sphinx-build コマンドの '-c' オプションを使用します。
残念ながら、Read the Docsはできません。
何か良い方法があれば教えてください。

## URL

* ParaviewのRTDプロジェクトページ:

  * https://readthedocs.org/projects/paraview/  (Master)
  * https://readthedocs.org/projects/paraview-ja/

* 各言語のドキュメントページ:

  * https://paraview.readthedocs.io/en/latest/
  * https://paraview.readthedocs.io/ja/latest/

## RTDで翻訳ドキュメントプロジェクトを立ち上げる方法

詳細はこちら: https://docs.readthedocs.org/en/latest/localization.html#project-with-multiple-translations

ポイントは:

* 各言語のRTDプロジェクトが必要です。
* 各プロジェクトは、 "Settings" ページで言語を正しく設定する必要があります。
* マスタープロジェクトは、 "translations settings" ページで各翻訳プロジェクトに接続されます。


## poファイルの更新方法

```
sh ./locale/update.sh
```

その後、更新された po ファイルをコミットしてください。


## 言語の追加方法

1. locale/update.shに言語を追加する:

   ```
   - rm -R ja
   - tx pull -l ja
   + rm -R ja de
   + tx pull -l ja,de
   ```

1. poファイルの更新

1. コミットする

1. Read The Docsに新しいプロジェクトを追加する:

   https://readthedocs.org/projects/paraview-ja/

1. 以下のように、親プロジェクトに翻訳プロジェクトを追加する:

   https://readthedocs.org/dashboard/paraview/translations/

<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="クリエイティブ・コモンズ・ライセンス" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a><br />この 作品 は <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">クリエイティブ・コモンズ 表示 - 改変禁止 4.0 国際 ライセンス</a>の下に提供されています。
